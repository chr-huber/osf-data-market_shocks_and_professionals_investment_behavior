## Market Shocks and Professionals' Investment Behavior – Evidence from the COVID-19 crash
Christoph Huber, Jürgen Huber, Michael Kirchler

---

This repository contains all data and analyses for '_Market shocks and professionals' investment behavior -- Evidence from the COVID-19 crash_'.

- `raw` contains the raw data files (raw oTree output)

- `notebook.nb.html` and `notebook.Rmd` contain the _R_-code replicating all analyses, tables, and figures in the paper

- Sensitivity analyses following Oster (2019) are conducted using the code in `sensitivity_analyses.do` in Stata

Make sure to download the data in `raw` with the same folder structure as in this folder before running the replication code. The files in `fonts` are needed for creating figures using the Bitstream Charter font.

The experiments were run using oTree by Chen et al. (2016).

All analyses were conducted using _R_.