
*------------------------------------------------------------------------------*
*--- Sensitivity analyses for 												---*
*--- 'Market shocks and professionals' investment behavior - Evidence from the *
*--- COVID-19 crahs'														---*
*--- Christoph Huber, Juergen Huber, Michael Kirchler						---*
*------------------------------------------------------------------------------*

* We use the psacalc function by Emily Oster (2019):
ssc install psacalc


clear all

use "sampling_reshaped_pool.dta", clear


* Comparable investment tasks - dependent variable "investment in risky asset":
*     - Kirchler et al. 2018: R-squared = 0.109 (Table I, Model (4))
*     - Ehm et al. 2014: adj. R-squared = 0.256 (Table V)
*     - Cohn et al. 2017: R-squared = 0.081 (Table 2, Model (2))
*                         R-squared = 0.101 (Table 4, Model (1))
*     - Nolte/Schneider 2018: R-squared = 0.059 (Table 6, Model (3))
*
* We therefore use a conservative R_max of 0.50.


* Table 3: Investment - Professionals - Pooled
preserve
	
	keep if pool1 == "prof"
	collapse (mean) investment risk_aversion1 risk_aversion2 crt age female, ///
		by(pool1 wave returns subject)
		
	reg investment i.wave risk_aversion1 risk_aversion2 crt age female, robust

	scalar rmaxx = 1.3*e(r2)
	di rmaxx
	psacalc delta 2.wave, rmax(.33974637)
	
restore

* Table 3: Investment - Professionals - Prices
preserve
	
	keep if pool1 == "prof" & returns == 1
	collapse (mean) investment risk_aversion1 risk_aversion2 crt age female, ///
		by(pool1 wave returns subject)
		
	reg investment i.wave risk_aversion1 risk_aversion2 crt age female, robust

	psacalc delta 2.wave
	psacalc delta 2.wave, rmax(0.5)
	
restore

* Table 3: Investment - Professionals - Returns
preserve
	
	keep if pool1 == "prof" & returns == 2
	collapse (mean) investment risk_aversion1 risk_aversion2 crt age female, ///
		by(pool1 wave returns subject)
		
	reg investment i.wave risk_aversion1 risk_aversion2 crt age female, robust

	psacalc delta 2.wave
	psacalc delta 2.wave, rmax(0.5)
	
restore




* Comparable tasks - dependent variable "risk perception":
*     - Holzmeister et al. 2020: adj. R-squared = 0.048 (Table 1, Model (1))
*     - Huber/Huber 2019: between R-squared = 0.076 (Table 1, Model (2))
*     - Borsboom/Zeisberger 2020: Pseudo R-squared = 0.179 (Table 4)
*
* We therefore use a conservative R_max of 0.35.

* Table 3: Risk perception - Professionals - Pooled
preserve
	
	keep if pool1 == "prof" 
	collapse (mean) risk risk_aversion1 risk_aversion2 crt age female, ///
		by(pool1 wave returns subject)
		
	reg risk i.wave risk_aversion1 risk_aversion2 crt age female, robust

	scalar rmaxx = 1.3 * e(r2)
	di rmaxx
	psacalc delta 2.wave, rmax(.09547743)	
	
restore

* Table 3: Investment - Professionals - Prices
preserve
	
	keep if pool1 == "prof" & returns == 1
	collapse (mean) risk risk_aversion1 risk_aversion2 crt age female, ///
		by(pool1 wave returns subject)
		
	reg risk i.wave risk_aversion1 risk_aversion2 crt age female, robust

	psacalc delta 2.wave
	psacalc delta 2.wave, rmax(0.35)
	
restore

* Table 3: Investment - Professionals - Returns
preserve
	
	keep if pool1 == "prof" & returns == 2
	collapse (mean) risk risk_aversion1 risk_aversion2 crt age female, ///
		by(pool1 wave returns subject)
		
	reg risk i.wave risk_aversion1 risk_aversion2 crt age female, robust

	psacalc delta 2.wave
	psacalc delta 2.wave, rmax(0.35)
	
restore







